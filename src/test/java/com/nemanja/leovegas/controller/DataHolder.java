package com.nemanja.leovegas.controller;

import com.nemanja.leovegas.dto.TransactionDto;
import com.nemanja.leovegas.entity.Account;
import com.nemanja.leovegas.entity.Player;
import com.nemanja.leovegas.entity.Transactions;

import java.math.BigDecimal;

/**
 * Nemanja.Tomanovic created on 14-Apr-20
 **/
class DataHolder {

    protected static Player getPlayer(){
        Player player = new Player();


        player.setAge(21);
        player.setEmail("bestplayerever@gmail.com");
        player.setName("John Doe");
        player.setPassword("verystrongpassword");
        player.setUserName("johnDoe");

        return player;
    }

    protected static Player getPlayerForTransaction(){
        Player player = new Player();


        player.setId(20L);
        player.setAge(42);
        player.setEmail("jimmy@gmail.com");
        player.setName("Jimmy Doe");
        player.setPassword("jimmypass");
        player.setUserName("jimmy");

        return player;
    }

    protected static Account getAccount(){
        Account account = new Account();
        account.setCurrency("USD");
        account.setBalance(BigDecimal.ZERO);
        account.setPlayer(DataHolder.getPlayer());
        return account;
    }

    protected static TransactionDto getTransactionSameId(){
        TransactionDto transaction = new TransactionDto();

        transaction.setId(1L);
        transaction.setAmount(BigDecimal.valueOf(-200));
        transaction.setPlayerId(getPlayerForTransaction().getId());

        return transaction;
    }

    protected static TransactionDto getTransaction(){
        TransactionDto transaction = new TransactionDto();

        transaction.setId(2L);
        transaction.setAmount(BigDecimal.valueOf(0));
        transaction.setPlayerId(getPlayerForTransaction().getId());

        return transaction;
    }
    protected static TransactionDto getTransactionWithHighDebitFunds(){
        TransactionDto transaction = new TransactionDto();

        transaction.setId(3L);
        transaction.setAmount(BigDecimal.valueOf(-200000));
        transaction.setPlayerId(getPlayerForTransaction().getId());

        return transaction;
    }

    protected static TransactionDto getTransactionBalanceCheckDebit(){
        TransactionDto transaction = new TransactionDto();

        transaction.setId(4L);
        transaction.setAmount(BigDecimal.valueOf(-200));
        transaction.setPlayerId(getPlayerForTransaction().getId());

        return transaction;
    }
    protected static Transactions getTransactionBalanceCheckCredit(){
        Transactions transaction = new Transactions();

        transaction.setId(4L);
        transaction.setAmount(BigDecimal.valueOf(200));
        transaction.setPlayer(getPlayerForTransaction());

        return transaction;
    }
}
