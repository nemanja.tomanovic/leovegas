package com.nemanja.leovegas.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
class AccountControllerTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    void setup(){
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void findBalance() throws Exception {

        Long playerId = 20L;
        mvc.perform(get("/balance/find/{playerId}", playerId)

            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON))

            .andExpect(status().isOk());
    }

    @Test
    void findBalanceNullId() throws Exception {

        Long playerId = null;
        mvc.perform(get("/balance/find/{playerId}", playerId)

                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))

                .andExpect(status().is4xxClientError());
    }

    @Test
    void findBalanceWrongId(){

        Long playerId = 23L;
        assertThatThrownBy(() ->  mvc.perform(get("/balance/find/{playerId}", playerId)

                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk()))
                .hasRootCause(new NullPointerException());

    }
}