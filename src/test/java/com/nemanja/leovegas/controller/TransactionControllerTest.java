package com.nemanja.leovegas.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nemanja.leovegas.dto.TransactionDto;
import com.nemanja.leovegas.exceptions.ExistingIdException;
import com.nemanja.leovegas.exceptions.InsufficientFundsException;
import com.nemanja.leovegas.service.AccountService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.math.BigDecimal;
import java.math.MathContext;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
class TransactionControllerTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private AccountService accountService;

    @BeforeEach
    void setup() {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void saveTransaction() throws Exception {
        mvc.perform(post("/transaction/save")

                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(DataHolder.getTransaction())))

                .andDo(MockMvcResultHandlers.print())

                .andExpect(status().isOk());


    }

    @Test
    void saveTransactionNonUniqueId() {
        assertThatThrownBy(() -> mvc.perform(post("/transaction/save")

                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(DataHolder.getTransactionSameId())))

                .andExpect(status().isOk()))
                .hasRootCause(new ExistingIdException("ID provided already exists"));
    }


    @Test
    void saveTransactionHighDebitFunds() {
        assertThatThrownBy(() -> mvc.perform(post("/transaction/save")

                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(DataHolder.getTransactionWithHighDebitFunds())))

                .andExpect(status().isOk()))
                .hasRootCause(new InsufficientFundsException("Withdrawal amount is higher then balance"));
    }

    @Test
    void saveTransactionCheckBalance() throws Exception {
        TransactionDto transaction = DataHolder.getTransactionBalanceCheckDebit();
        mvc.perform(post("/transaction/save")

                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(transaction)))

                .andDo(MockMvcResultHandlers.print())

                .andExpect(status().isOk());

        MathContext m = new MathContext(0);

        BigDecimal expectedBalance = BigDecimal.valueOf(800.00).round(m);
        BigDecimal actualBalance = accountService.findAccountByPlayerId(transaction.getPlayerId()).getAmount().round(m);

        assertEquals(0, expectedBalance.compareTo(actualBalance));

    }

    @Test
    void findTransactions() throws Exception {
        Long playerId = 20L;
        mvc.perform(get("/transaction/find/{playerId}", playerId)

                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))

                .andDo(MockMvcResultHandlers.print())

                .andExpect(jsonPath("$", Matchers.notNullValue()))
                .andExpect(jsonPath("$[0].player.id", Matchers.is(playerId.intValue())))
                .andExpect(jsonPath("$[0].amount", Matchers.is(500.0)))
                .andExpect(status().isOk());


    }
}