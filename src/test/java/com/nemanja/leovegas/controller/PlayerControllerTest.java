package com.nemanja.leovegas.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.nemanja.leovegas.entity.Player;
import com.nemanja.leovegas.exceptions.InvalidParametersExceptions;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
class PlayerControllerTest {


    private MockMvc mvc;

    @Autowired
    private WebApplicationContext context;

    @BeforeEach
    void setup(){
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    void savePlayer() throws Exception {

        mvc.perform(post("/player/save")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(DataHolder.getPlayer())))

                .andExpect(jsonPath("$", Matchers.notNullValue()))
                .andExpect(jsonPath("$.age", Matchers.is(21)))

                .andExpect(status().isOk());

    }

    @Test
    void savePlayerValidationErrorAge() {
        Player player = DataHolder.getPlayer();
        player.setAge(11);

        assertThatThrownBy(() -> mvc.perform(post("/player/save")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(player))))
                .hasRootCause(new InvalidParametersExceptions("Invalid validation for parameters passed!"));
    }

    @Test
    void savePlayerValidationErrorEmail() {
        Player player = DataHolder.getPlayer();
        player.setEmail("testmail");

        assertThatThrownBy(() -> mvc.perform(post("/player/save")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(player))))
                .hasRootCause(new InvalidParametersExceptions("Invalid validation for parameters passed!"));
    }
}