insert into player(id, name, user_name, password, age, email) VALUES (20, 'Jenny Doe', 'jenny', 'jennypass', 35, 'jenny@gmail.com');

insert into account(id, player_id, balance, currency) VALUES (20, 20, 1000, 'EUR');

insert into transactions (id, player_id, amount) values (1, 20, 500);