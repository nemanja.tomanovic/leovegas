package com.nemanja.leovegas.controller;

import com.nemanja.leovegas.entity.Account;
import com.nemanja.leovegas.entity.Player;
import com.nemanja.leovegas.exceptions.InvalidParametersExceptions;
import com.nemanja.leovegas.service.AccountService;
import com.nemanja.leovegas.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.math.BigDecimal;

/**
 * Nemanja.Tomanovic created on 14-Apr-20
 **/
@RestController
public class PlayerController {

    private PlayerService playerService;
    private AccountService accountService;

    @Autowired
    public PlayerController(PlayerService playerService, AccountService accountService) {
        this.playerService = playerService;
        this.accountService = accountService;
    }

    @RequestMapping(value = "/player/save", method = RequestMethod.POST)
    public Player savePlayer(@Valid @RequestBody Player player, BindingResult bindingResult) {

           if (!bindingResult.hasErrors()){
               Player savedPlayer = playerService.savePlayer(player);

               Account account = new Account();
               account.setBalance(BigDecimal.ZERO);
               account.setCurrency("EUR");
               account.setPlayer(player);
               accountService.saveAccount(account);

               return savedPlayer;
           }else{
               throw new InvalidParametersExceptions("Invalid validation for parameters passed!");
           }
    }
}
