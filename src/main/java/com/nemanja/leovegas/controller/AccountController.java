package com.nemanja.leovegas.controller;

import com.nemanja.leovegas.dto.AccountDto;
import com.nemanja.leovegas.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Nemanja.Tomanovic created on 14-Apr-20
 **/
@RestController
public class AccountController {


    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @RequestMapping(value = "/balance/find/{playerId}", method = RequestMethod.GET)
    public AccountDto findBalance(@PathVariable("playerId")Long playerId) {
        return accountService.findAccountByPlayerId(playerId);
    }
}
