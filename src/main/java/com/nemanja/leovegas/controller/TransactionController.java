package com.nemanja.leovegas.controller;

import com.nemanja.leovegas.dto.TransactionDto;
import com.nemanja.leovegas.entity.Transactions;
import com.nemanja.leovegas.exceptions.InvalidParametersExceptions;
import com.nemanja.leovegas.service.PlayerService;
import com.nemanja.leovegas.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


/**
 * Nemanja.Tomanovic created on 12-Apr-20
 **/
@RestController
public class TransactionController {

    private TransactionService transactionService;
    private PlayerService playerService;

    @Autowired
    public TransactionController(TransactionService transactionService, PlayerService playerService) {
        this.transactionService = transactionService;
        this.playerService = playerService;
    }

    @RequestMapping(value = "/transaction/save", method = RequestMethod.POST)
    public Transactions saveTransaction(@Valid @RequestBody TransactionDto transaction, BindingResult bindingResult) {

        if (!bindingResult.hasErrors()){
            Transactions transactions = new Transactions();
            transactions.setAmount(transaction.getAmount());
            transactions.setId(transaction.getId());
            transactions.setPlayer(playerService.findPlayer(transaction.getPlayerId()));

            return transactionService.saveTransaction(transactions);
        }else{
            throw new InvalidParametersExceptions("Invalid validation for parameters passed!");
        }
    }

    @RequestMapping(value = "/transaction/find/{playerId}", method = RequestMethod.GET)
    public List<Transactions> findTransactions(@PathVariable("playerId")Long playerId){
        return transactionService.findTransactionsByPlayerId(playerId);
    }
}
