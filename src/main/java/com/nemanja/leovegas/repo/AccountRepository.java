package com.nemanja.leovegas.repo;

import com.nemanja.leovegas.entity.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account, Long> {

    Account findByPlayerId(Long playerId);
}
