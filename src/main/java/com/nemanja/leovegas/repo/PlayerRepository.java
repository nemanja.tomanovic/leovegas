package com.nemanja.leovegas.repo;

import com.nemanja.leovegas.entity.Player;
import org.springframework.data.repository.CrudRepository;

public interface PlayerRepository extends CrudRepository<Player, Long> {
}
