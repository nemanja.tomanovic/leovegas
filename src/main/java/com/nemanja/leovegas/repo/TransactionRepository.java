package com.nemanja.leovegas.repo;

import com.nemanja.leovegas.entity.Transactions;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<Transactions, Long> {

    List<Transactions> findByPlayerId(Long playerId);
}
