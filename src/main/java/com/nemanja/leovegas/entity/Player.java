package com.nemanja.leovegas.entity;

import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * Nemanja.Tomanovic created on 07-Apr-20
 **/
@Entity
@Table(name = "player")
public class Player extends BaseEntity {

    @Column(name = "name")
    @NotBlank
    @Size(max = 32)
    private String name;
    @Column(name = "user_name")
    @NotBlank
    @Size(min = 4, max = 18)
    private String userName;
    @Column(name = "password")
    @NotBlank
    @Size(min = 8, max = 32)
    private String password;
    @Column(name = "age")
    @NotNull
    @Min(18)
    private int age;
    @Column(name = "email")
    @Email
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


}
