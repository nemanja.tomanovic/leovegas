package com.nemanja.leovegas.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Nemanja.Tomanovic created on 12-Apr-20
 **/
@Entity
@Table(name = "transactions")
public class Transactions {

    @Column(name = "id", unique = true)
    @Id
    private Long id;
    @ManyToOne
    private Player player;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "dateCreated")
    @CreationTimestamp
    private Timestamp createdDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

}
