package com.nemanja.leovegas.entity;


import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Nemanja.Tomanovic created on 12-Apr-20
 **/
@Entity
@Table(name = "account")
public class Account extends BaseEntity {


    @OneToOne()
    @JoinColumn(name = "playerId")
    private Player player;
    @Column(name = "balance")
    private BigDecimal balance;
    @Column(name = "currency")
    private String currency;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
