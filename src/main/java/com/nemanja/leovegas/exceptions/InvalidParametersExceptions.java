package com.nemanja.leovegas.exceptions;

/**
 * Nemanja.Tomanovic created on 20-Apr-20
 **/
public class InvalidParametersExceptions extends RuntimeException{

    public InvalidParametersExceptions(String message) {
        super(message);
    }

    public InvalidParametersExceptions(String message, Throwable cause) {
        super(message, cause);
    }
}
