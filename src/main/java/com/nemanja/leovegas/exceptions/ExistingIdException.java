package com.nemanja.leovegas.exceptions;

/**
 * Nemanja.Tomanovic created on 20-Apr-20
 **/
public class ExistingIdException extends RuntimeException {

    public ExistingIdException(String message) {
        super(message);
    }

    public ExistingIdException(String message, Throwable cause) {
        super(message, cause);
    }
}
