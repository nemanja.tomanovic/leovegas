package com.nemanja.leovegas.exceptions;


/**
 * Nemanja.Tomanovic created on 13-Apr-20
 **/
public class InsufficientFundsException extends RuntimeException {

    public InsufficientFundsException(String errorMessage) {
        super(errorMessage);
    }

    public InsufficientFundsException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }

}
