package com.nemanja.leovegas.dto;

import java.math.BigDecimal;

/**
 * Nemanja.Tomanovic created on 14-Apr-20
 **/
public class AccountDto {

    private Long playerId;
    private BigDecimal amount;

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
