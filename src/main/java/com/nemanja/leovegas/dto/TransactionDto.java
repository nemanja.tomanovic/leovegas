package com.nemanja.leovegas.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Nemanja.Tomanovic created on 20-Apr-20
 **/
public class TransactionDto {

    private Long id;
    private Long playerId;
    @NotNull
    private BigDecimal amount;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
