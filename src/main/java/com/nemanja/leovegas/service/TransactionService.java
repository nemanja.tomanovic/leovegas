package com.nemanja.leovegas.service;

import com.nemanja.leovegas.entity.Transactions;

import java.util.List;

public interface TransactionService {
    Transactions saveTransaction(Transactions transactions);
    Transactions findTransactionsById(Long id);
    List<Transactions> findTransactionsByPlayerId(Long playerId);
    boolean checkId(Long id);
}
