package com.nemanja.leovegas.service;

import com.nemanja.leovegas.dto.AccountDto;
import com.nemanja.leovegas.entity.Account;

public interface AccountService {
    Account saveAccount(Account account);
    AccountDto findAccountByPlayerId(Long playerId);
}
