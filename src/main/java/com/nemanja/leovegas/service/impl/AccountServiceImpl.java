package com.nemanja.leovegas.service.impl;

import com.nemanja.leovegas.dto.AccountDto;
import com.nemanja.leovegas.entity.Account;
import com.nemanja.leovegas.repo.AccountRepository;
import com.nemanja.leovegas.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Nemanja.Tomanovic created on 12-Apr-20
 **/
@Service
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }

    @Override
    public AccountDto findAccountByPlayerId(Long playerId) {
        Account account = accountRepository.findByPlayerId(playerId);
        AccountDto accountDto = new AccountDto();
        accountDto.setAmount(account.getBalance());
        accountDto.setPlayerId(playerId);
        return accountDto;
    }
}
