package com.nemanja.leovegas.service.impl;

import com.nemanja.leovegas.entity.Account;
import com.nemanja.leovegas.entity.Transactions;
import com.nemanja.leovegas.exceptions.ExistingIdException;
import com.nemanja.leovegas.exceptions.InsufficientFundsException;
import com.nemanja.leovegas.repo.AccountRepository;
import com.nemanja.leovegas.repo.TransactionRepository;
import com.nemanja.leovegas.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Nemanja.Tomanovic created on 12-Apr-20
 **/
@Service
public class TransactionServiceImpl implements TransactionService {

    private TransactionRepository transactionRepository;
    private AccountRepository accountRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public Transactions saveTransaction(Transactions transactions) {
        Account account = accountRepository.findByPlayerId(transactions.getPlayer().getId());
        BigDecimal newBalance = account.getBalance().add(transactions.getAmount());
        if (transactions.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            //debit transaction
            if (newBalance.compareTo(BigDecimal.ZERO) >= 0) {
                // got enough funds
                return validateAndSaveTransaction(transactions, account, newBalance);
            } else {
                throw new InsufficientFundsException("Withdrawal amount is higher then balance");
            }
        }
        else {
            return validateAndSaveTransaction(transactions, account, newBalance);
        }
    }


    @Override
    public Transactions findTransactionsById(Long id) {
        Optional<Transactions> transactions = transactionRepository.findById(id);
        return transactions.orElseGet(Transactions::new);
    }

    @Override
    public List<Transactions> findTransactionsByPlayerId(Long playerId) {
        return transactionRepository.findByPlayerId(playerId);
    }

    @Override
    public boolean checkId(Long id) {
        Transactions dbTrx = transactionRepository.findById(id).orElseGet(Transactions::new);
        return dbTrx.getId() != null;
    }

    private Transactions validateAndSaveTransaction(Transactions transactions, Account account, BigDecimal newBalance) {
        if (!checkId(transactions.getId())) {
            Transactions trx = transactionRepository.save(transactions);
            account.setBalance(newBalance);
            accountRepository.save(account);
            return trx;
        } else {
            throw new ExistingIdException("ID provided already exists");
        }
    }
}
