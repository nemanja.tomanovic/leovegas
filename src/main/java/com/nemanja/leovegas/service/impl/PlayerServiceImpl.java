package com.nemanja.leovegas.service.impl;

import com.nemanja.leovegas.entity.Player;
import com.nemanja.leovegas.repo.PlayerRepository;
import com.nemanja.leovegas.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Nemanja.Tomanovic created on 12-Apr-20
 **/
@Service
public class PlayerServiceImpl implements PlayerService {

    private PlayerRepository playerRepository;

    @Autowired
    public PlayerServiceImpl(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Override
    public Player savePlayer(Player player) {
        return playerRepository.save(player);
    }

    @Override
    public Player findPlayer(Long id) {
        return playerRepository.findById(id).orElseGet(Player::new);
    }
}
