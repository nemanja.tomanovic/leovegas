package com.nemanja.leovegas.service;

import com.nemanja.leovegas.entity.Player;

public interface PlayerService {
    Player savePlayer(Player player);
    Player findPlayer(Long id);
}
