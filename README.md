This application is created for leovegas recruitment assignment.

Persistance technology used was spring-data with H2 database.

Test were done using MockMVC/junit5.

There are few custom exceptions created for purpose of better explaining failure that occured.

Solution persists data across restarts(as it was described in bonus part of assignment) under file storedData.db on classpath.

Database runs on http://localhost:8088/h2 with username: sa, empty password and JDBC url jdbc:h2:file:./storedData

Test database is stored in file testData.db on classpath, it is also H2 database with username: leovegas, password: vegasleo and JDBC url
jdbc:h2:file:./testData.

For fresh data just delete those two files.

All necessary configuration is in application.properties(for app) and test.application(for tests).